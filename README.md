# 简述
> 简单的时间格式转换小插件


> 格式有年y月M日d时h分m秒s 毫秒S 星期一到星期日W 季度q  


# 用法

```javascript
const date_sdk = require('date-sdk')

let date = new Date().format('yyyy-MM-dd hh:mm:ss W')//可以是年月日时分秒
console.log(date)
```